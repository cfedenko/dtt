# -*- coding: UTF-8 -*-
import sys
import pygtk
if not sys.platform == 'win32':
    pygtk.require('2.0')

import gtk
import gobject
import settings

class StartFrame(gtk.VBox):
    __gsignals__ = {
        'start_test': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_INT,))
    }
    def on_start_button_clicked(self, widget):
        self.emit("start_test", self.spinner.get_value_as_int()-1)    
        
    def __init__(self):
        gtk.VBox.__init__(self)       
                
        hbox1 = gtk.HBox()
        self.pack_start(hbox1, fill=False)
        
        vbox = gtk.VBox(spacing=10)
        hbox1.pack_start(vbox, fill=False)
        
        hbox2 = gtk.HBox()
        vbox.pack_start(hbox2, False)
        
        paper_label = gtk.Label(u"Билет:")        
        paper_label.set_alignment(0.0,0.5)        
        hbox2.add(paper_label)
        
        adj = gtk.Adjustment(1.0, 1.0, 93.0, 1.0, 5.0, 0.0)        
        self.spinner = gtk.SpinButton(adj)        
        self.spinner.set_wrap(True)        
        hbox2.add(self.spinner)
        
        self.start_button = gtk.Button(u"Для работы нажмите здесь")
        self.start_button.set_name("start-button")
        
        self.start_button.connect("clicked", self.on_start_button_clicked)
        self.start_button.set_size_request(240, settings.button_height)
        vbox.pack_start(self.start_button, False)
