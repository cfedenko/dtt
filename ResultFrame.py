# -*- coding: UTF-8 -*-
import sys
import pygtk
if not sys.platform == 'win32':
    pygtk.require('2.0')

import gtk
import gobject
import settings

class ResultFrame(gtk.VBox):
    __gsignals__ = {
        'return': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'quit': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
    }

    def set_result(self, result):
        self.result_label.set_markup(settings.result_massages[result[0]])
        
    def __init__(self):
        gtk.VBox.__init__(self)
        self.result_label = gtk.Label()
        self.add(self.result_label)
        buttonbox = gtk.HButtonBox()
        buttonbox.set_spacing(10)
        buttonbox.set_layout(gtk.BUTTONBOX_CENTER)
        self.pack_start(buttonbox, False)
        
        return_button = gtk.Button(u"Тест")
        return_button.set_size_request(-1, settings.button_height)
        return_button.connect("clicked", lambda w: self.emit("return"))
        buttonbox.add(return_button)
        quit_button = gtk.Button(u"Выход")
        quit_button.set_size_request(-1, settings.button_height)
        quit_button.connect("clicked", lambda w: self.emit("quit"))
        buttonbox.add(quit_button)
