from distutils.core import setup
import py2exe
import string

Mydata_files = []

for i in range(93):
    f1 = "data/" + string.zfill(i+1, 2)
    f2 = []
    for j in range(20):
        f2.append(f1 + "/" + str(j+1) + ".jpg")
    f2.append(f1 + "/a.txt")    
    Mydata_files.append((f1, f2))
 
setup(
   windows=[{"script":"dtt.py"}],
   data_files = Mydata_files,
   options={"py2exe": {"packages":"encodings", "includes":["cairo", "gio", "pango", "atk", "pangocairo"]}}
)

