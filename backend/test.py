#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import sys, re, os.path
import string
import settings


class Paper:
    def __init__(self, paper):
        self.answers = []
        for i in range(20): self.answers.append(None)
        self.wrong_answers = 0
        self.leftover_questions = range(20)
        self.index = 0
        self.img =[]
        self.num =[]
        self.cor =[]
        self.paper_dir = os.path.join(settings.data_dir, string.zfill(paper+1, 2))
        file_name = os.path.join(self.paper_dir, 'a.txt')
        
        f = open(file_name, 'rb')
        lines = f.readlines()
        f.close()
        for i in range(20):
            m1 = re.search("(\S+)\r\n", lines[i*2])
            m2 = re.search("(\d+) (\d+)\r\n", lines[i*2+1])
            self.img.append(m1.group(1))
            self.num.append(int(m2.group(2)) - 1)
            self.cor.append(int(m2.group(1)) - 1)
        
    def get_next_question(self):
        if not self.is_exam_end():
            return self.leftover_questions[self.index]
        else:
            return None
        
    def get_leftover_questions(self):
        return len(self.leftover_questions)                   
              
    def is_exam_end(self):
        return self.leftover_questions == []   
        
    def get_img_num(self, question):
        return os.path.join(self.paper_dir, self.img[question]), self.num[question]
        
    def set_answer(self, answer):
        cur_question = self.leftover_questions[self.index]
        if answer == -1:
            self.index = (self.index + 1) % len(self.leftover_questions)
            return cur_question, None
        else:
            self.leftover_questions.remove(self.leftover_questions[self.index])
            if not self.is_exam_end():
                self.index = self.index  % len(self.leftover_questions)
            cor = self.cor[cur_question] == answer
            if not cor:
                self.wrong_answers += 1   
            self.answers[cur_question] = cor
            return cur_question, cor
        
    def get_answers(self):
        return self.answers
        
    def is_exam_failed(self):
        return self.wrong_answers > 2          
         




