import pygtk
pygtk.require("2.0")
import gtk
from gtk import DrawingArea

class PaperBox(DrawingArea):
    
    def __init__(self, backcolor=None, bordercolor=None):
        DrawingArea.__init__(self)
        self.active = False
        self.borderwidth = 6
        self.bordercolor = bordercolor
        self.backcolor = backcolor
        self.connect('expose_event', self.expose)
    
    def invalidate(self):
        self.queue_draw()
        
    def set_active(self, active):
        self.active = active
        self.invalidate()
        
    def set_borderwidth(self, borderwidth):
        self.borderwidth = borderwidth
        self.invalidate()    
        
    def set_bordercolor(self, bordercolor):
        self.bordercolor = bordercolor
        self.invalidate()        
        
    def set_backcolor(self, backcolor):
        self.backcolor = backcolor
        self.invalidate()
        
        
        
    def expose(self, widget, event):
        # Load Cairo drawing context.
        self.context = self.window.cairo_create()
        # Set a clip region.
        self.context.rectangle(
            event.area.x, event.area.y,
            event.area.width, event.area.height)
        self.context.clip()
        # Render image.
        self.draw(self.context)
        return False

    def draw(self, context):
        # Get dimensions.
        rect = self.get_allocation()
        x, y = 0, 0
#        x, y = rect.x, rect.y
#        # Remove parent offset, if any.
#        parent = self.get_parent()
#        if parent:
#            offset = parent.get_allocation()
#            x -= offset.x
#            y -= offset.y
        # Fill background color.
        
        if self.active:
            if self.bordercolor:
                context.set_source_color(self.bordercolor)
            else:
                color = self.get_style().base[gtk.STATE_SELECTED]
                context.set_source_color(color)
                del color                                   
                       
            context.rectangle(x, y, rect.width, rect.height)
            context.fill()            
        if self.backcolor:
            context.set_source_color(self.backcolor)
            context.rectangle(self.borderwidth, self.borderwidth,
                              rect.width-self.borderwidth*2,
                              rect.height-self.borderwidth*2)
            context.fill()


