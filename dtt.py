﻿#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import sys
import pygtk
if not sys.platform == 'win32':
    pygtk.require('2.0')

import gtk

from StartFrame import StartFrame
from TestFrame import TestFrame
from ResultFrame import ResultFrame

class App:

    def on_quit(self, widget, data=None):
        gtk.main_quit()

    def on_start_test(self, widget, paper):
        self.window.remove(self.start_frame)
        self.window.add(self.test_frame)
        self.window.show_all()
        self.test_frame.run(paper)

    def on_return(self, widget):
        self.window.remove(self.result_frame)
        self.window.add(self.start_frame)
        self.window.show_all()

    def on_test_end(self, widget):
        print self.test_frame.get_result()
        self.window.remove(self.test_frame)
        self.window.add(self.result_frame)
        self.result_frame.set_result(self.test_frame.get_result())
        self.window.show_all()

    def __init__(self):

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_border_width(10)
        self.window.connect("delete_event", self.on_quit)

        self.start_frame = StartFrame()
        self.start_frame.connect("start_test", self.on_start_test)
        self.window.add(self.start_frame)

        self.test_frame = TestFrame()
        self.test_frame.connect("test_end", self.on_test_end)

        self.result_frame = ResultFrame()
        self.result_frame.connect("quit", self.on_quit)
        self.result_frame.connect("return", self.on_return)

        self.window.show_all()
        self.window.fullscreen()
        #self.window.maximize()

        if sys.platform == 'win32':
            window_size = self.window.get_size()
            self.window.set_size_request(*window_size)

def main():
    gtk.main()
    return 0

if __name__ == "__main__":
    App()
    main()
