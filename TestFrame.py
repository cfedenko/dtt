# -*- coding: UTF-8 -*-
import sys
import pygtk
if not sys.platform == 'win32':
    pygtk.require('2.0')

import gtk
import gobject
import string

from widgets.resizableimage import ResizableImage
from widgets.paperbox import PaperBox
from backend.test import Paper
import settings

colors = [gtk.gdk.color_parse('#CC0000'),
          gtk.gdk.color_parse('#4E9A06'),
          gtk.gdk.color_parse('#204A87')]
          
class TestFrame(gtk.VBox):
    __gsignals__ = {
        'test_end': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
    }    
    
    def on_answer_button_clicked(self, widget, data):
        question, result = self.paper.set_answer(data)       
        if result == 1:
            self.paperboxes[question].set_backcolor(colors[result])
        elif result == 0:
            self.paperboxes[question].set_backcolor(colors[result])
            
        self.apdate_labels()
            
        if self.paper.is_exam_failed():
            self.stop_timer = True
            self.result = False
            self.emit("test_end") 
                        
        if self.paper.is_exam_end():
            self.stop_timer = True
            self.result = True
            self.emit("test_end")
                   
        else:
            self.show_question(self.paper.get_next_question())
            
                        
    def show_question(self, question):
        img_path, num = self.paper.get_img_num(question)
        
        self.image.set_from_file(img_path)
        
        for i in range(5):
            if i <= num:
                self.answer_buttons[i].show()
            else:
                self.answer_buttons[i].hide()
                
        for i in range(20):
            self.paperboxes[i].set_active(i == question)
        
    def apdate_labels(self):
        self.label1.set_text(u"Вы ответили на %d" %
                             (20 - self.paper.get_leftover_questions()))
        self.label2.set_text(u"Осталось ответить на %d" %
                             self.paper.get_leftover_questions())
                
    def on_timer(self):
        self.time += 1
        self.time_label.set_text(u"Время теста: %s:%s" %
                                 (string.zfill(self.time / 60, 2),
                                  string.zfill(self.time % 60, 2)))        
        
        # if self.time >= 1200:
        if self.stop_timer:
            return False
        elif self.time >= 1200:
            self.result = self.paper.get_leftover_questions() <= 2
            self.emit("test_end")    
            return False            
        else:
            return True
            
    def run(self, n):
        self.paper = Paper(n)
        self.time = 0
        self.stop_timer = False
        self.result = None
        for i in range(20):
            self.paperboxes[i].set_backcolor(colors[2])
        self.show_question(0)
        self.apdate_labels()
        self.paper_label.set_text(u"Билет: %d" % (n+1))
        timer = gobject.timeout_add(1000, self.on_timer)
                
    def get_result(self):
        return self.result, self.time, self.paper.get_answers()      
   

    def __init__(self):
        gtk.VBox.__init__(self, spacing=10)
    
        hbox1 = gtk.HBox()
        #hbox1.set_homogeneous(True)
        self.pack_start(hbox1, False)
                
        self.paper_label = gtk.Label(u"Билет: 1")
        hbox1.pack_start(self.paper_label, False)
        
        self.time_label = gtk.Label(u"Время теста: 00:00")
        hbox1.pack_end(self.time_label, False)
        
        frame0 = gtk.Frame()
        frame0.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.add(frame0)
        
        vbox1 = gtk.VBox(spacing=10)
        vbox1.set_border_width(10)
        frame0.add(vbox1)
        
        hbox2 = gtk.HBox(spacing=10)
        vbox1.pack_start(hbox2)
        
        vbox2 = gtk.VBox()
        hbox2.pack_start(vbox2, False)
                
        button_box = gtk.VButtonBox()
        button_box.set_layout(gtk.BUTTONBOX_START)
        #button_box.set_layout(gtk.BUTTONBOX_CENTER)
        button_box.set_spacing(10)        
       
        button_box.set_size_request(-1, settings.button_height * 5 + 10 * 4)
        
        vbox2.pack_start(button_box, fill=False)
                
        self.answer_buttons = []
        for i in range(5):
            self.answer_buttons.append(gtk.Button())
            self.answer_buttons[i].set_size_request(120, settings.button_height)
            self.answer_buttons[i].connect("clicked", self.on_answer_button_clicked, i)
            self.answer_buttons[i].set_name("answer-button")
            image = gtk.Image()
            image.set_from_file("./img/%d.png" % (i+1))
            self.answer_buttons[i].add(image)
            button_box.add(self.answer_buttons[i])
            
        self.image = ResizableImage(interp = gtk.gdk.INTERP_BILINEAR,
                                    backcolor=gtk.gdk.color_parse(settings.paper_backcolor))
        self.image.set_size_request(100, 100)
        
        hbox2.add(self.image)
        
        alignment1 = gtk.Alignment(0.5,0.5, 1, 1)
        vbox1.pack_start(alignment1, expand=False)
                
        hbox3 = gtk.HBox(spacing=10)
        hbox3.set_homogeneous(True)       
        alignment1.add(hbox3)
        
        frame1 = gtk.Frame()
        frame1.set_shadow_type(gtk.SHADOW_OUT)
        hbox3.add(frame1)
        
        self.label1 = gtk.Label()         
        frame1.add(self.label1)
        
        frame2 = gtk.Frame()
        frame2.set_shadow_type(gtk.SHADOW_OUT)
        hbox3.add(frame2)
        
        self.label2 = gtk.Label()
        frame2.add(self.label2)
        
        skip_button = gtk.Button(u"Пропустить")
        skip_button.set_size_request(-1, settings.button_height)
        skip_button.connect("clicked", self.on_answer_button_clicked, -1)
        hbox3.add(skip_button)
        
        hbox4 = gtk.HBox()        
        self.pack_end(hbox4, False)
                
        table = gtk.Table(2, 20)
        hbox4.pack_start(table, True, False)
               
        self.paperboxes = []
        
        for i in range(20):
            self.paperboxes.append(PaperBox())
            self.paperboxes[i].set_size_request(39, 39)
            #self.paperboxes[i].set_size_request(30, 30)
            self.paperboxes[i].set_borderwidth(4)
            table.attach(self.paperboxes[i], i, i+1, 0, 1)
            table.attach(gtk.Label(str(i+1)), i, i+1, 1, 02)
                        
