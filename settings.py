# -*- coding: UTF-8 -*-
import sys

# 842x587

if sys.platform == 'win32':
    data_dir = r".\data"
else:
    data_dir = "./data"

button_height = 35

box_colors = ['#CC0000',
              '#4E9A06',
              '#204A87']
          
paper_backcolor = '#FFFFFF'          

result_massages = [
"""<span weight="bold" foreground="#CC0000" size="xx-large">Тест не сдан</span>""",
"""<span weight="bold" foreground="#4E9A06" size="xx-large">Тест сдан</span>"""]
